package com.pms.sdk;

public interface IPMSConsts {
	
	public static final String PMS_VERSION = "2.0";

	public final static String PMS_PROPERY_NAME = "pms.properties";

	// [start] GCM
	public static final String KEY_APP = "app";
	public static final String KEY_SENDER = "sender";
	public static final String KEY_GCM_PROJECT_ID = "gcm_project_id";
	public static final String KEY_GCM_TOKEN = "registration_id";
	public static final String DB_UUID = "uuid";

	public static final String URL_GOOGLE_CLIENT = "https://www.google.com/accounts/ClientLogin";
	public static final String URL_GOOGLE_SENDER = "https://android.apis.google.com/c2dm/send";

	public static final String ACTION_REGISTER = "com.google.android.c2dm.intent.REGISTER";
	public static final String ACTION_REGISTRATION = "com.google.android.c2dm.intent.REGISTRATION";
	public static final String ACTION_RECEIVE = "com.google.android.c2dm.intent.RECEIVE";
	public static final String GSF_PACKAGE = "com.google.android.gsf";
	
	public static final String RECEIVE_PERMISSION = "com.google.android.c2dm.permission.SEND";
	
	public static final String GCMRECIVER_PATH = "com.pms.sdk.push.gcm.GCMReceiver";
	// [end] GCM
	
	
	// [start] PUSH
	public static final String KEY_MSG_ID = "i";
	public static final String KEY_NOTI_TITLE = "notiTitle";
	public static final String KEY_NOTI_MSG = "notiMsg";
	public static final String KEY_NOTI_IMG = "notiImg";
	public static final String KEY_MSG = "message";
	public static final String KEY_SOUND = "sound";
	public static final String KEY_MSG_TYPE = "t";
	public static final String KEY_DATA = "d";
	// [end] PUSH
	
	
	// [start] Receiver
	public static final String RECEIVER_PUSH = "com.pms.sdk.push";
	public static final String RECEIVER_REQUERY = "com.pms.sdk.requery";
//	public static final String RECEIVER_REFRESH_MSG_GRP = "com.pms.sdk.refreshmsggrp";
//	public static final String RECEIVER_REFRESH_MSG = "com.pms.sdk.refreshmsg";
//	public static final String RECEIVER_REFRESH_BADGE = "com.pms.sdk.refreshbadge";
	public static final String RECEIVER_CLOSE_INBOX = "com.pms.sdk.closeinbox";
	public static final String RECEIVER_NOTIFICATION = "com.pms.sdk.notification";
	public static final String DEFAULT_PUSH_POPUP_ACTIVITY = "com.pms.sdk.push.PushPopupActivity";
	// [end] Receiver
	
	
	// [start] Preferences
	public static final String PREF_FILE_NAME = "pref_pms";
	
	public static final String FLAG_Y = "Y";
	public static final String FLAG_N = "N";
	
	public static final String PREF_CUST_ID = "cust_id";
	public static final String PREF_OTN_ID = "otn_id";
	public static final String PREF_MAX_USER_MSG_ID = "max_user_msg_id";
	
	public static final String PREF_MQTT_FLAG = "mqtt_flag";
	
	public static final String PREF_CLICK_LIST = "click_list";
	public static final String PREF_READ_LIST = "read_list";
	
	public static final String PREF_MSG_FLAG = "msg_flag";
	public static final String PREF_NOTI_FLAG = "noti_flag";
	public static final String PREF_CONTENT_NOTI_FLAG = "content_noti_flag";
	public static final String PREF_EVENT_NOTI_FLAG = "event_noti_flag";
	public static final String PREF_ALERT_FLAG = "alert_flag";
	public static final String PREF_RING_FLAG = "ring_flag";
	public static final String PREF_VIBE_FLAG = "vibe_flag";
	public static final String PREF_SCREEN_WAKEUP_FLAG = "screen_wakeup_flag";
	
	public static final String PREF_INBOX_ACTIVITY = "pref_inbox_activity";
	public static final String PREF_PUSH_POPUP_ACTIVITY = "pref_push_popup_activity";
	public static final String PREF_PUSH_POPUP_COLOR = "pref_push_popup_color";
	public static final String PREF_PUSH_POPUP_BACKGROUND_COLOR = "pref_push_popup_background_color";
	
	public static final String PREF_POPUP_CONFIRM_TEXT = "popup_confirm_text";
	public static final String PREF_POPUP_CANCEL_TEXT = "popup_cancel_text";
	
	public static final String PREF_NOTI_ICON = "noti_icon";
	public static final String PREF_LARGE_NOTI_ICON = "noti_large_icon";
	public static final String PREF_NOTI_SOUND = "noti_sound";
	public static final String PREF_NOTI_RECEIVER = "noti_receiver";
	// [end] Preferences
	
	
	// [start] api
	// default encrypt key
	public static final String DEFAULT_ENC_KEY = "Pg-s_E_n_C_k_e_y";
	// API SERVER URL
	public static final String API_SERVER_URL = "http://119.207.76.92:8092/msg-api/";
	// time
	public static final long EXPIRE_RETAINED_TIME = 30/*minute*/ * 60 * 1000;
	public static final String PREF_LAST_API_TIME = "pref_last_api_time";
	// URL list
	public static final String API_DEVICE_CERT = "deviceCert.m";
	public static final String API_LOGIN_PMS = "loginPms.m";
	public static final String API_NEW_MSG = "newMsg.m";
	public static final String API_READ_MSG = "readMsg.m";
	public static final String API_CLICK_MSG = "clickMsg.m";
	public static final String API_SET_CONFIG = "setConfig.m";
	public static final String API_GET_CONFIG = "getConfig.m";
	public static final String API_LOGOUT_PMS = "logoutPms.m";
	// pref
	public static final String PREF_SERVER_URL = "pref_server_url";
	public static final String PREF_APP_USER_ID = "pref_app_user_id";
	public static final String PREF_ENC_KEY = "pref_enc_key";
	// request params(json) key
	public static final String KEY_API_DEFAULT = "d";
	public static final String KEY_APP_USER_ID = "id";
	public static final String KEY_ENC_PARAM = "data";
	// result params(json) key
	public static final String KEY_API_CODE = "code";
	public static final String KEY_API_MSG  = "msg";
	// result code (server)
	public static final String CODE_SUCCESS = "000";
	public static final String CODE_NULL_PARAM = "100";
	public static final String CODE_WRONG_PARAM = "101";
	public static final String CODE_DECRPYT_FAIL = "104";
	public static final String CODE_WRONG_SESSION = "105";
	public static final String CODE_INNER_ERROR = "200";
	public static final String CODE_EXTRA_ERROR = "201";
	// result code (client)
	public static final String CODE_SESSION_EXPIRED = "901";
	public static final String CODE_CONNECTION_ERROR = "902";
	public static final String CODE_NOT_RESPONSE = "903";
	public static final String CODE_URL_IS_NULL = "904";
	public static final String CODE_PARAMS_IS_NULL = "905";
	// device cert
	public static final String NO_TOKEN = "noToken";
	// new msg type
	public static final String TYPE_PREV = "P";
	public static final String TYPE_NEXT = "N";
	// [end] api

	// [start] Property key
	public static final String PRO_ENABLE_TMS_UUID = "enable_PmsUUID";
	// [end] Property key
}
