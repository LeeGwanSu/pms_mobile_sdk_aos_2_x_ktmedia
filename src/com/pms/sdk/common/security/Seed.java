package com.pms.sdk.common.security;

import java.io.ByteArrayOutputStream;

public class Seed {

	private static byte[] _STATIC_USER_KEY = {(byte)0xab, (byte)0x12, (byte)0xc1, (byte)0x45, (byte)0x31, (byte)0x3a, (byte)0xdf, (byte)0xf0,
        (byte)0x55, (byte)0x4a, (byte)0x62, (byte)0x2c, (byte)0xcc, (byte)0xa3, (byte)0x73, (byte)0xdf}; 
	
	public static void setStaticUserKey( byte[] userKey ) {
		System.arraycopy(userKey, 0, _STATIC_USER_KEY, 0, 16);
	}
	
	public static byte[] seedEncrypt(byte[] base64text) throws Exception {
		return seedEncrypt( base64text, _STATIC_USER_KEY);
	}

	public static byte[] seedEncrypt(byte[] base64text, byte[] userKey) throws Exception {
		
		int pdwRoundKey[] = new int[32];
		byte pbData[] = new byte[16];
		byte pbCipher[]   = new byte[16];
		
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		
		seedx.SeedRoundKey(pdwRoundKey, userKey);
		
		for( int pos=0; pos<base64text.length; pos+=16) {
			for( int i=0; i<16; i++) pbData[i] = (byte)0x00;
			System.arraycopy(base64text, pos, pbData, 0, (base64text.length-pos)>16?16:base64text.length-pos);
			seedx.SeedEncrypt( pbData, pdwRoundKey, pbCipher);
			byteBuffer.write(pbCipher);
		}
		
		return byteBuffer.toByteArray();
	}


	public static byte[] seedDecrypt(byte[] seedtext) throws Exception {
		
		return seedDecrypt(seedtext, _STATIC_USER_KEY);
	}

	public static byte[] seedDecrypt(byte[] seedText, byte[] userKey) throws Exception {
		
		int pdwRoundKey[] = new int[32];
		byte pbData[] = new byte[16];
		byte pbCipher[]   = new byte[16];
		
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		
		seedx.SeedRoundKey(pdwRoundKey, userKey);
		
		for( int pos=0; pos<seedText.length; pos+=16) {
			for( int i=0; i<16; i++) pbData[i] = (byte)0x00;
			System.arraycopy(seedText, pos, pbData, 0, (seedText.length-pos)>16?16:seedText.length-pos);
			seedx.SeedDecrypt( pbData, pdwRoundKey, pbCipher);
			byteBuffer.write(pbCipher);
		}
		
		return byteBuffer.toByteArray();
	}

}
