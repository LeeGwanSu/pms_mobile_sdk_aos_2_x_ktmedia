package com.pms.sdk.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

	private static final String SD_CARD_ROOT = "/sdcard/";
	
	/**
	 * write to sd card
	 * @param dir
	 * @param fileName
	 * @param data
	 * @throws IOException
	 */
	public void writeToSdCard(String dir, String fileName, String data) throws Exception {
		if (new File(SD_CARD_ROOT + dir).mkdir()) {
		} else {
		}
		
		File file = new File(SD_CARD_ROOT + dir, fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		
		FileWriter fw = new FileWriter(file, true);
		BufferedWriter bufferWritter = new BufferedWriter(fw);
		bufferWritter.write(data);
		bufferWritter.close();
	}
	
	/**
	 * read from sd card
	 * @param dir
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public String readFromSdCard(String dir, String fileName) throws Exception {
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new FileReader(SD_CARD_ROOT + dir + fileName));

		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}

		br.close();
        return sb.toString();
	}
	
	/**
	 * get file name list
	 * @param dir
	 * @param search
	 * @return
	 */
	public List<String> getFileNameListFromSdCard(String dir, String search) {
		List<String> filePathList = new ArrayList<String>();
		File directory = new File(SD_CARD_ROOT + dir);
		if (directory.exists()) {
			for (File file : directory.listFiles()) {
				if (search != null && !"".equals(search)) {
					if (file.getName().indexOf(search) > -1) {
						filePathList.add(file.getName());
					}
				} else {
					filePathList.add(file.getName());
				}
			}
		}
		return filePathList;
	}
	
}
