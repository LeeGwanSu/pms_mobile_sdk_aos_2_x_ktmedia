package com.pms.sdk.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @since 2012.01.14
 * @author erzisk
 * @description date util
 */
public class DateUtil {
	
	public static final String DEFAULT_FORMAT = "yyyyMMddkkmmss";
	
	public static String getNowDate() {
        SimpleDateFormat sdf = new SimpleDateFormat( );
        sdf.applyPattern(DEFAULT_FORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
//        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format( new Date().getTime() );
	}
	
	public static long changeDateToMilliSecond(String dateStr) {
		if (dateStr == null || dateStr.trim().equals("") || dateStr.length() != 14) {
			return 0;
		}
		try {
			SimpleDateFormat format = new SimpleDateFormat(DEFAULT_FORMAT);
			return format.parse(dateStr).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	
	/**
	 * SimpleDateFormat을 사용하여 date converting
	 * @param date<br>ex) "20120812230908"
	 * @param inputFromat<br>ex) "yyyyMMddkkmmss"
	 * @param outputFormat<br>ex) "yyyy년 M월 d일 E요일"
	 * @return
	 */
	public static String convertDate(String date, String inputFromat, String outputFormat) {
		SimpleDateFormat originalFormat = new SimpleDateFormat(inputFromat);
		SimpleDateFormat newFormat = new SimpleDateFormat(outputFormat);
		
		try {
			Date originalDate = originalFormat.parse(date);
			return newFormat.format(originalDate);
			
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	public static String convertDate(Calendar c, String outputFormat) {
		try {
			SimpleDateFormat newFormat = new SimpleDateFormat(outputFormat);
			return newFormat.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * convert chat date
	 * @param timeString<br> ex)20120101010101
	 * @return
	 * 날짜가 현재와 같을 경우 : Am 01:01 or Pm 01:01
	 * <br>
	 * 날짜가 현재와 다를 경우 : 2012.01.01
	 */
	public static String convertChatDate(String timeString) {
		try {
			if (timeString.substring(0, 8).equals(getNowDate().substring(0, 8))) {
				return convertDate(timeString, DEFAULT_FORMAT, "aa hh:mm");
			} else {
				return convertDate(timeString, DEFAULT_FORMAT, "yyyy년 MM월 dd일");
			}
		} catch (StringIndexOutOfBoundsException e) {
			return "";
		}
		
	}
}
