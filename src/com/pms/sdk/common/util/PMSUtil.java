package com.pms.sdk.common.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.PowerManager;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;

/**
 * 
 * @author erzisk
 * @since 2013.06.26
 */
public class PMSUtil implements IPMSConsts {

	/**
	 * set mqtt flag
	 * @param context
	 * @param mqttFlag
	 */
	public static void setMQTTFlag(final Context context, boolean mqttFlag) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_MQTT_FLAG, mqttFlag ? FLAG_Y : FLAG_N);
	}
	
	/**
	 * get mqtt flag
	 * @param context
	 * @return
	 */
	public static String getMQTTFlag(final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String mqttFlag = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get("PMS_MQTT_FLAG");
			return mqttFlag == null || mqttFlag.equals("") ? prefs.getString(PREF_MQTT_FLAG) : mqttFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}
	
	/**
	 * return application key
	 * @param context
	 * @return
	 */
	public static String getApplicationKey(final Context context) {
		try {
			return (String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get("PMS_APP_KEY");
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	/**
	 * return server url
	 * <br>AndroidManifest.xml에 SERVER_URL이 셋팅되어 있지 않거나,
	 * <br>meta data를 불러오는중 exception이 발생하면
	 * <br>APIVariable.SERVER_URL을 return
	 * @param context
	 * @return
	 */
	public static String getServerUrl(final Context context) {
		try {
			Prefs prefs = new Prefs(context);
			String serverUrl = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get("PMS_API_SERVER_URL");
			return serverUrl == null || serverUrl.equals("") ? prefs.getString(PREF_SERVER_URL) : serverUrl;
		} catch (Exception e) {
			e.printStackTrace();
			return API_SERVER_URL;
		}
	}
	
	/**
	 * set server url
	 * @param context
	 * @param serverUrl
	 */
	public static void setServerUrl(final Context context, String serverUrl) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_SERVER_URL, serverUrl);
	}
	
	/**
	 * set gcm project id
	 * @param context
	 * @param projectId
	 */
	public static void setGCMProjectId(final Context context, String projectId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(KEY_GCM_PROJECT_ID, projectId);
	}
	
	/**
	 * get gcm project id
	 * @param context
	 * @return
	 */
	public static String getGCMProjectId(final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(KEY_GCM_PROJECT_ID);
	}
	
	/**
	 * set gcm token
	 * @param context
	 * @param gcmToken
	 */
	public static void setGCMToken(final Context context, String gcmToken) {
		Prefs prefs = new Prefs(context);
		prefs.putString(KEY_GCM_TOKEN, gcmToken);
	}
	
	/**
	 * return gcm token (push)
	 * @param context
	 * @return
	 */
	public static String getGCMToken(final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(KEY_GCM_TOKEN);
	}
	
	/**
	 * set encrypt key
	 * @param context
	 * @param encKey
	 */
	public static void setEncKey(final Context context, String encKey) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_ENC_KEY, encKey);
	}
	
	/**
	 * get encrypt key
	 * @param context
	 * @return
	 */
	public static String getEncKey(final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_ENC_KEY);
	}

	/**
	 * get mqtt flag
	 *
	 * @param context
	 * @return
	 */
	public static String getEnableUUIDFlag (final Context context) {
		try {
			String uuidFlag = (String) context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA).metaData
					.get("PMS_ENABLE_UUID");
			return uuidFlag == null ? FLAG_Y : uuidFlag;
		} catch (Exception e) {
			e.printStackTrace();
			return FLAG_N;
		}
	}


	/**
	 * set UUID
	 *
	 * @param context
	 * @param gcmToken
	 */
	public static void setUUID (final Context context, String gcmToken) {
		// 20170811 fixed by hklim
		// UUID 저장 시에 Shared Preference 를 사용하지 않고 DB 사용 하도록 수정됨
//		Prefs prefs = new Prefs(context);
//		prefs.putString(PREF_UUID, gcmToken);
		DataKeyUtil.setDBKey(context, DB_UUID, gcmToken);
	}

	/**
	 * get UUID
	 *
	 * @param context
	 * @return
	 */
	public static String getUUID (final Context context) {
		Prefs prefs = new Prefs(context);
//		return prefs.getString(PREF_UUID);
		String strReturn = prefs.getString(PREF_UUID);
		if (strReturn == null || strReturn.isEmpty())
			strReturn = DataKeyUtil.getDBKey(context, DB_UUID);
		return strReturn;
	}

	/**
	 * set appUserId
	 * @param context
	 * @param appUserId
	 */
	public static void setAppUserId(final Context context, String appUserId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_APP_USER_ID, appUserId);
	}
	
	/**
	 * get appUserId
	 * @param context
	 * @return
	 */
	public static String getAppUserId(final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_APP_USER_ID);
	}
	
	/**
	 * set cust id
	 * @param context
	 * @param custId
	 */
	public static void setCustId(final Context context, String custId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_CUST_ID, custId);
	}
	
	/**
	 * get cust id
	 * @param context
	 * @return
	 */
	public static String getCustId(final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_CUST_ID);
	}
	
	/**
	 * set otn id
	 * @param context
	 * @param custId
	 */
	public static void setOtnId(final Context context, String otnId) {
		Prefs prefs = new Prefs(context);
		prefs.putString(PREF_OTN_ID, otnId);
	}
	
	/**
	 * get otn id
	 * @param context
	 * @return
	 */
	public static String getOtnId(final Context context) {
		Prefs prefs = new Prefs(context);
		return prefs.getString(PREF_OTN_ID);
	}
	
	/**
	 * get Link From html
	 * @param htmlStr
	 * @return
	 */
	public static Map<Integer, String> getLinkFromHtml(String htmlStr) {
		Map<Integer, String> map = new HashMap<Integer, String>();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)(http[^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			map.put(i, m.group(2));
		}
		return map;
	}
	
	/**
	 * replaceLink
	 * @param htmlStr
	 * @return
	 */
	public static String replaceLink(String htmlStr) {
		StringBuffer s = new StringBuffer();
		Pattern p = Pattern.compile("<a\\s*href\\s*=\\s*([\"']*)(http[^\"'>\\s]+)\\1(.*?)>(.*?)</a>");
		Matcher m = p.matcher(htmlStr);
		for (int i = 1; m.find(); i++) {
			m.appendReplacement(s, m.group(0).replace(m.group(2), "click://" + i));
		}
		m.appendTail(s);
		return s.toString();
	}
	
	
	/**
	 * find top activity
	 * @return
	 */
    public static String[] findTopActivity(Context c) {
    	String[] top = new String[2];
    	ActivityManager activityManager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
    	List<RunningTaskInfo> info;
    	info = activityManager.getRunningTasks(1);
    	for (Iterator<RunningTaskInfo> iterator = info.iterator(); iterator.hasNext(); ) {
    		RunningTaskInfo runningTaskInfo = (RunningTaskInfo) iterator.next();
    		
    		top[0] = runningTaskInfo.topActivity.getPackageName();
    		top[1] = runningTaskInfo.topActivity.getClassName();
    	}
    	
    	return top;
    }
    
    /**
     * runnedApp
     * 현재 어플리케이션이 실행중인지 여부
     * @return
     */
    public static boolean isRunnedApp(Context c) {
    	if (findTopActivity(c)[0].equals(c.getPackageName())) {
    		return true;
        } else {
        	return false;
        }
    }
    
    
    /**
     * is screen on
     * @param c
     * @return
     */
    public static boolean isScreenOn(Context c) {
    	PowerManager pm;
    	try {
    		pm = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
    		return pm.isScreenOn();
    	} catch (Exception e) {
    		return false;
    	} finally {
    		PMS.clear();
    	}
    }
    
    /**
     * array to preferences
     * @param obj
     */
    public static void arrayToPrefs(Context c, String key, Object obj) {
    	Prefs prefs = new Prefs(c);
    	String arrayString = prefs.getString(key);
    	JSONArray array = null;
    	try {
        	if ("".equals(arrayString)) {
        		array = new JSONArray();
        	} else {
        		array = new JSONArray(arrayString);
        	}
        	array.put(obj);
        	prefs.putString(key, array.toString());
    	} catch (Exception e) {
//    		e.printStackTrace();
    	}
    }
    
    /**
     * array from preferences
     * @param c
     * @param key
     * @return
     */
    public static JSONArray arrayFromPrefs(Context c, String key) {
    	Prefs prefs = new Prefs(c);
    	String arrayString = prefs.getString(key);
    	JSONArray array = new JSONArray();
    	try {
    		array = new JSONArray(arrayString);
    	} catch (Exception e) {
//    		e.printStackTrace();
    	}
    	return array;
    }
}
