package com.pms.sdk.common.util;

import android.util.Log;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description log (custom)
 */
public class CLog {
	
	private static String TAG = "PMS";
	private static boolean debugMode = false;
	
	public static void setTagName(String tagName) {
		CLog.TAG = tagName;
	}
	public static void setDebugMode(boolean debugMode) {
		CLog.debugMode = debugMode;
	}

	/**
	 * verbose
	 * 
	 * @param str
	 */
	public static void v(String str) {
		if (debugMode) {
			Log.v(TAG, str);
		}
	}

	/**
	 * debug
	 * 
	 * @param str
	 */
	public static void d(String str) {
		if (debugMode) {
			Log.d(TAG, str);
		}
	}

	/**
	 * info
	 * 
	 * @param str
	 */
	public static void i(String str) {
//		if (debugMode) {
			Log.i(TAG, str);
//		}
	}

	/**
	 * warn
	 * 
	 * @param str
	 */
	public static void w(String str) {
//		if (debugMode) {
			Log.w(TAG, str);
//		}
	}

	/**
	 * error
	 * 
	 * @param str
	 */
	public static void e(String str) {
//		if (debugMode) {
			Log.e(TAG, str);
//		}
	}
}
