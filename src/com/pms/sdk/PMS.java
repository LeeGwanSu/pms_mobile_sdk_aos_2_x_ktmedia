package com.pms.sdk;

import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;


/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version
 * [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 * [2013.10.11] android-query -> volley 로 library 변경<br>
 * [2013.10.14] push notification 출력 변경<br>
 * [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 * [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 * [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 * [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 * [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 * [2013.10.28 10:37] setConfig flag값 추가, getConfig class 추가 <br>
 * [2013.10.28 10:42] PMS Class에 getContentNotiFlag(), getEventNotiFlag() 메소드 추가 및 initOption()의 셋팅값 추가 
 * 					  DeviceCert Class에 위 셋팅갑 저장 부분 추가.<br>
 * [2013.10.28 11:20] getConfig.m 중 UserAppId Null 이라면 DeviceCert.m을 호출하여 저장된 값을 가지고 초기화 시키는 부분 추가 <br>
 * [2013.10.29 14:30] PMS Class에 setOtnId(), getOtnId() 추가
 * 					  DeviceCert Class에 getParam()중 setOtnId 값 추가
 * 					  LoginPms Class에 getParam()중 setOtnId 값 추가
 * 					  PMSUtil Class에 setOtnId(), getOtnId() 추가 <br>
 * [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 * [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 * [2013.12.27 15:05] GCM regist 시에 GCM_PACKAGE 전송 <br>
 */
public class PMS implements IPMSConsts {
	
	private static final String TAG = "[" + PMS.class.getSimpleName() + "]";
	
	// auth status
	public enum AUTH {
		PENDING, PROGRESS, FAIL, COMPLETE;
	}
	
	private AUTH authStatus = AUTH.PENDING;
	
	private static PMS instance;
	private Context mContext;
	
	private PMSDB mDB;
	private Prefs mPrefs;
	
	private OnReceivePushListener onReceivePushListener;
	
	/**
	 * constructor
	 * @param context
	 */
	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i(TAG + "Version:" + PMS_VERSION + ",UpdateDate:201312271505");
		
		initOption(context);
		
		/**
		 * refresh db thread
		 */
		new Thread(new Runnable() {
			@Override
			public void run() {
				CLog.i(TAG + "refreshDBHandler:start");
				mDB.deleteExpireMsg();
				mDB.deleteEmptyMsgGrp();
				if (mContext != null) {
					mContext.sendBroadcast(new Intent(RECEIVER_REQUERY));
				}
				CLog.i(TAG + "refreshDBHandler:end");
			}
		}).start();
	}
	
	/**
	 * initialize option
	 */
	private void initOption(Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
    		mPrefs.putString(PREF_NOTI_FLAG, "Y");
    	}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
    		mPrefs.putString(PREF_MSG_FLAG, "Y");
    	}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_CONTENT_NOTI_FLAG))) {
    		mPrefs.putString(PREF_CONTENT_NOTI_FLAG, "Y");
    	}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_EVENT_NOTI_FLAG))) {
    		mPrefs.putString(PREF_EVENT_NOTI_FLAG, "Y");
    	}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
    		mPrefs.putString(PREF_RING_FLAG, "Y");
    	}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
    		mPrefs.putString(PREF_VIBE_FLAG, "Y");
    	}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
    		mPrefs.putString(PREF_ALERT_FLAG, "Y");
    	}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CANCEL_TEXT))) {
			mPrefs.putString(PREF_POPUP_CANCEL_TEXT, "CLOSE");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CONFIRM_TEXT))) {
			mPrefs.putString(PREF_POPUP_CONFIRM_TEXT, "OK");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
	}
	
	/**
	 * getInstance
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance(Context context, String projectId) {
		PMSUtil.setGCMProjectId(context, projectId);
		return getInstance(context);
	}
	
	/**
	 * getInstance
	 * @param context
	 * @return
	 */
	public static PMS getInstance(Context context) {
		CLog.d(TAG + "getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		if (instance == null) {
			instance = new PMS(context);
			if (PhoneState.isAvailablePush()) {
				PushReceiver.gcmRegister(context, PMSUtil.getGCMProjectId(context));
			}
		}
		instance.setmContext(context);
		return instance;
	}
	
	/**
	 * start mqtt service
	 */
	public void startMQTTService(Context context) {
		context.sendBroadcast(new Intent(context, RestartReceiver.class));
	}
	
	public void stopMQTTService(Context context) {
		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent Intent = new Intent(context, RestartReceiver.class);
		PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, Intent, 0);
        alarmManager.cancel(pIntent);
        
		context.stopService(new Intent(context, MQTTService.class));
	}
	
	/**
	 * clear
	 * @return
	 */
	public static boolean clear() {
		try {
			instance.authStatus = AUTH.PENDING;
			instance.unregisterReceiver();
			instance = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	private void setmContext(Context context) {
		this.mContext = context;
	}
	
	private void unregisterReceiver() {
		Badge.getInstance(mContext).unregisterReceiver();
	}
	
//	/**
//	 * push token 세팅
//	 * @param pushToken
//	 */
//	public void setPushToken(String pushToken) {
//		CLog.i(TAG + "setPushToken");
//		CLog.d(TAG + "setPushToken:pushToken=" + pushToken);
//		PMSUtil.setGCMToken(mContext, pushToken);
//	}
	
	/**
	 * set ServerUrl
	 * @param serverUrl
	 */
	public void setServerUrl(String serverUrl) {
		CLog.i(TAG + "setServerUrl");
		CLog.d(TAG + "setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}
	
	/**
	 * otn id 세팅
	 * @param otnId
	 */
	public void setOtnId(String otnId)
	{
		CLog.i(TAG + "setOtnId");
		PMSUtil.setOtnId(mContext, otnId);
	}
	
	/**
	 * get otn id
	 * @return
	 */
	public String getOtnId() 
	{
		CLog.i(TAG + "getOtnId");
		return PMSUtil.getOtnId(mContext);
	}
	
	/**
	 * cust id 세팅
	 * @param custId
	 */
	public void setCustId(String custId) {
		CLog.i(TAG + "setCustId");
		CLog.d(TAG + "setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}
	
	/**
	 * get cust id
	 * @return
	 */
	public String getCustId() {
		CLog.i(TAG + "getCustId");
		return PMSUtil.getCustId(mContext);
	}
	
	/**
	 * set inbox activity
	 * @param inboxActivity
	 */
	public void setInboxActivity(String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}
	
	/**
	 * set push popup activity
	 * @param pushPopupActivity
	 */
	public void setPushPopupActivity(String pushPopupActivity) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, pushPopupActivity);
	}
	
	/**
	 * set pushPopup color
	 * @param pushPopupColor
	 */
	public void setPushPopupColor(int pushPopupColor) {
		mPrefs.putInt(PREF_PUSH_POPUP_COLOR, pushPopupColor);
	}
	
	/**
	 * set pushPopup background color
	 * @param pushPopupBackgroundColor
	 */
	public void setPushPopupBackgroundColor(int pushPopupBackgroundColor) {
		mPrefs.putInt(PREF_PUSH_POPUP_BACKGROUND_COLOR, pushPopupBackgroundColor);
	}
	
	/**
	 * get msg flag
	 * @return
	 */
	public String getMsgFlag() {
		return mPrefs.getString(PREF_MSG_FLAG);
	}
	
	/**
	 * get noti flag
	 * @return
	 */
	public String getNotiFlag() {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}
	
	/**
	 * get content noti flag
	 * @return
	 */
	public String getContentNotiFlag() {
		return mPrefs.getString(PREF_CONTENT_NOTI_FLAG);
	}
	
	/**
	 * get event noti flag
	 * @return
	 */
	public String getEventNotiFlag() {
		return mPrefs.getString(PREF_EVENT_NOTI_FLAG);
	}
	
	/**
	 * set noti icon
	 * @param resId
	 */
	public void setNotiIcon(int resId) {
		mPrefs.putInt(PREF_NOTI_ICON, resId);
	}
	
	/**
	 * set large noti icon
	 * @param resId
	 */
	public void setLargeNotiIcon(int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}
	
	/**
	 * set noti cound
	 * @param resId
	 */
	public void setNotiSound(int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}
	
	/**
	 * set noti receiver
	 * @param classPath
	 */
	public void setNotiReceiver(String classPath) {
		mPrefs.putString(PREF_NOTI_RECEIVER, classPath);
	}
	
	/**
	 * set ring mode
	 * @param isRingMode
	 */
	public void setRingMode(boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}
	
	/**
	 * set vibe mode
	 * @param isVibeMode
	 */
	public void setVibeMode(boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}
	
	/**
	 * set popup noti
	 * @param isShowPopup
	 */
	public void setPopupNoti(boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}
	
	/**
	 * set screen wakeup
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup(boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}
	
	public OnReceivePushListener getOnReceivePushListener() {
		return onReceivePushListener;
	}
	public void setOnReceivePushListener(OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * pms device 인증
	 * @return
	 */
	public void authorize() {
		CLog.i(TAG + "authorize:start");
		// auth status setting
		authStatus = AUTH.PROGRESS;
		
		new DeviceCert(mContext).request(null, new APICallback() {
			@Override
			public void response(String code, JSONObject json) {
				
				if (CODE_SUCCESS.equals(code)) {
					// success device cert
					CLog.i(TAG + "DeviceCert:success device cert");
					authStatus = AUTH.COMPLETE;
				} else {
					// fail device cert
					CLog.i(TAG + "DeviceCert:fail device cert");
					authStatus = AUTH.FAIL;
				}
			}
		});
	}
	
	/**
	 * set debugTag
	 * @param tagName
	 */
	public void setDebugTAG(String tagName) {
		CLog.setTagName(tagName);
	}
	/**
	 * set debug mode
	 * @param debugMode
	 */
	public void setDebugMode(boolean debugMode) {
		CLog.setDebugMode(debugMode);
	}
	
	public void bindBadge(Context c, int id) {
		Badge.getInstance(c).addBadge(c, id);
	}
	public void bindBadge(TextView badge) {
		Badge.getInstance(badge.getContext()).addBadge(badge);
	}
	
	/**
	 * show Message Box
	 * @param c
	 */
	public void showMsgBox(Context c) {
		showMsgBox(c, null);
	}
	public void showMsgBox(Context c, Bundle extras) {
		CLog.i(TAG + "showMsgBox");
		if (PhoneState.isAvailablePush()) {
			if (authStatus == AUTH.PENDING || authStatus == AUTH.FAIL) {
				// auth가 유효한 상태가 아니라면 authrize진행
				CLog.i(TAG + "showMsgBox:no authorized");
				authorize();
			}
			
			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));
				
				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
//			Intent i = new Intent(INBOX_ACTIVITY);
//			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			if (extras != null) {
//				i.putExtras(extras);
//			}
//			c.startActivity(i);
		}
	}
	public void closeMsgBox(Context c) {
		Intent i = new Intent(RECEIVER_CLOSE_INBOX);
		c.sendBroadcast(i);
	}

	
	/* =====================================================
	 * [start] database
	 ===================================================== */
	/**
	 * select MsgGrp list
	 * @return
	 */
	public Cursor selectMsgGrpList() {
		return mDB.selectMsgGrpList();
	}
	/**
	 * select MsgGrp
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp(String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}
	/**
	 * select new msg Cnt
	 * @return
	 */
	public int selectNewMsgCnt() {
		return mDB.selectNewMsgCnt();
	}
	/**
	 * select Msg List
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList(int page, int row) {
		return mDB.selectMsgList(page, row);
	}
	/**
	 * select Msg List
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList(String msgCode) {
		return mDB.selectMsgList(msgCode);
	}
	/**
	 * select Msg
	 * @param userMsgId
	 * @return
	 */
	public Msg selectMsg(String userMsgId) {
		return mDB.selectMsg(userMsgId);
	}
	/**
	 * update MsgGrp
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp(String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}
	/**
	 * update read msg
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg(String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}
	/**
	 * update read msg where userMsgId
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId(String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}
	/**
	 * update read msg where msgId
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId(String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}
	/**
	 * delete Msg
	 * @param userMsgId
	 * @return
	 */
	public long deleteMsg(String userMsgId) {
		return mDB.deleteMsg(userMsgId);
	}
	/**
	 * delete MsgGrp
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp(String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}
	
	/**
	 * delete expire Msg
	 * @return
	 */
	public long deleteExpireMsg() {
		return mDB.deleteExpireMsg();
	}
	
	/**
	 * delete empty MsgGrp
	 * @return
	 */
	public void deleteEmptyMsgGrp() {
		mDB.deleteEmptyMsgGrp();
	}
	
	/**
	 * delete all
	 */
	public void deleteAll() {
		mDB.deleteAll();
	}
	
	/* =====================================================
	 * [end] database
	 ===================================================== */

	/**
	 * set popup confirm text
	 * @param confirmText
	 */
	public void setPopupConfirmText(String confirmText) {
		mPrefs.putString(PREF_POPUP_CONFIRM_TEXT, confirmText);
	}

	/**
	 * set popup cancel text
	 * @param cancelText
	 */
	public void setPopupCancelText(String cancelText) {
		mPrefs.putString(PREF_POPUP_CANCEL_TEXT, cancelText);
	}

}
