package com.pms.sdk.push;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;

/**
 * push popup activity
 * @author erzisk
 * @since 2013.06.07
 */
public class PushPopupActivity extends Activity implements IPMSConsts {
	
	private static final String TAG = "[" + PushPopupActivity.class.getSimpleName() + "]";
	
	private Context mContext;
	
	private FrameLayout mLayBack;
	private FrameLayout mLayOuter;
	private Prefs mPrefs;
	
	private PushMsg pushMsg;
	
	private int pushPopupColor = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		
		mPrefs = new Prefs(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		
		pushPopupColor = mPrefs.getInt(PREF_PUSH_POPUP_COLOR);
		
		int[] deviceSize = PhoneState.getDeviceSize(this);
		int maxWidth = (int) Math.round((deviceSize[0] < deviceSize[1] ? deviceSize[0] : deviceSize[1]) * 0.8);
		// set view
		// back이 가장 바깥, outer가 contents
		mLayBack = new FrameLayout(this);
		mLayOuter = new FrameLayout(this);
		
		mLayBack.setLayoutParams(new LayoutParams(-2, -1));
		mLayBack.setPadding(50, 50, 50, 50);
		mLayBack.setBackgroundColor(mPrefs.getInt(PREF_PUSH_POPUP_BACKGROUND_COLOR));
		LayoutParams lp = new LayoutParams(maxWidth, -2);
		lp.gravity = Gravity.CENTER;
		
		mLayOuter.setLayoutParams(lp);
		
		mLayBack.addView(mLayOuter);
		setContentView(mLayBack);
		
		CLog.i(TAG + "pushPopupColor:" + pushPopupColor);
		CLog.i(TAG + "pushPopupBackgroundColor:" + mPrefs.getInt(PREF_PUSH_POPUP_BACKGROUND_COLOR));
		setPushPopup(getIntent());
		
//		// screen on
//		pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//		wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Tag");
//		if (!pm.isScreenOn()) {
//			mLayBack.setBackgroundColor(0xFF000000);
//			wl.acquire();
//		}
//		
//		mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		this.overridePendingTransition(0, 0);
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		setPushPopup(intent);
	}
	
	/**
	 * set rich popup
	 * @param intent
	 */
	private void setPushPopup(Intent intent) {
		pushMsg = new PushMsg(intent.getExtras());
		
		CLog.i(TAG + pushMsg);
		mLayOuter.removeAllViews();
		
		if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
			mLayOuter.addView(getRichPopup(mContext, pushMsg.message));
		} else {
			mLayOuter.addView(getTextPopup(mContext, pushMsg.notiMsg));
		}
	}
	
	/**
	 * get rich popup
	 * @param context
	 * @param url
	 * @return
	 */
	private View getRichPopup(Context context, String url) {
		CLog.i(TAG + "getRichPopup");
		
		LinearLayout.LayoutParams lllp1 = new LinearLayout.LayoutParams(0, -2);
		lllp1.weight = 1;
		LinearLayout.LayoutParams lllp2 = new LinearLayout.LayoutParams(-1, -2);
		lllp2.weight = 1;
		
		LinearLayout layBack = new LinearLayout(context);
		layBack.setOrientation(LinearLayout.VERTICAL);
		layBack.setLayoutParams(new LayoutParams(-1, -1));
		layBack.setBackgroundColor(pushPopupColor);
		
		LinearLayout layButton = new LinearLayout(context);
		layButton.setOrientation(LinearLayout.HORIZONTAL);
		layButton.setLayoutParams(new LayoutParams(-1, -2));
		Button btnOk = new Button(context);
		
		String confirmText = mPrefs.getString(PREF_POPUP_CONFIRM_TEXT);
		String cancelText = mPrefs.getString(PREF_POPUP_CANCEL_TEXT);
		
		if (StringUtil.isEmpty(confirmText)) {
			confirmText = "OK";
		}
		if (StringUtil.isEmpty(cancelText)) {
			cancelText = "CLOSE";
		}
		
		btnOk.setText(confirmText);
		Button btnCancel = new Button(context);
		btnCancel.setText(cancelText);
		
		WebView wv = new WebView(context);
		wv.setLayoutParams(lllp2);
		
		final ProgressBar prg = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
		prg.setProgress(50);
		prg.setLayoutParams(lllp2);
		
		layButton.addView(btnCancel, lllp1);
		layButton.addView(btnOk, lllp1);
		
		layBack.addView(wv);
		layBack.addView(prg);
		layBack.addView(layButton);

		
		wv.setInitialScale(1);
		wv.setBackgroundColor(0);
		wv.setWebChromeClient(new WebChromeClient(){
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				prg.setProgress(newProgress);
				if (newProgress >= 100) {
					prg.setVisibility(View.GONE);
				}
			}
		});
		wv.setWebViewClient(webViewClient);

		WebSettings settings = wv.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setSupportMultipleWindows(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setPluginsEnabled(true);
		settings.setPluginState(PluginState.ON);
		settings.setRenderPriority(RenderPriority.HIGH);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setDomStorageEnabled(true);
		
		// setting html to webview
		if (Msg.TYPE_L.equals(pushMsg.msgType) && url.startsWith("http")) {
			wv.loadUrl(url);
		} else {
			wv.loadData(url, "text/html; charset=utf-8", null );
		}
		btnOk.setOnClickListener(onClickOk);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		return layBack;
	}
	
	/**
	 * webViewClient
	 */
	private WebViewClient webViewClient = new WebViewClient() {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
//			Intent intent = new Intent(Intent.ACTION_VIEW);
//			Uri uri = Uri.parse(url);
//			intent.setData(uri);
//			startActivity(intent);
			return super.shouldOverrideUrlLoading(view, url);
		}
	};
	
	/**
	 * get Text popup
	 * @param context
	 * @param url
	 * @return
	 */
	private View getTextPopup(Context context, String msg) {
		CLog.i(TAG + "getTextPopup");
		
		LinearLayout.LayoutParams lllp1 = new LinearLayout.LayoutParams(0, -2);
		lllp1.weight = 1;
		LinearLayout.LayoutParams lllp2 = new LinearLayout.LayoutParams(-1, -2);
		lllp2.setMargins(15, 15, 15, 15);
		
		LinearLayout layBack = new LinearLayout(context);
		layBack.setOrientation(LinearLayout.VERTICAL);
		layBack.setLayoutParams(new LayoutParams(-1, -1));
		layBack.setBackgroundColor(pushPopupColor);
		
		LinearLayout layButton = new LinearLayout(context);
		layButton.setOrientation(LinearLayout.HORIZONTAL);
		layButton.setLayoutParams(new LayoutParams(-1, -2));
		Button btnOk = new Button(context);
		btnOk.setText(mPrefs.getString(PREF_POPUP_CONFIRM_TEXT));
		Button btnCancel = new Button(context);
		btnCancel.setText(mPrefs.getString(PREF_POPUP_CANCEL_TEXT));
		
		TextView txt = new TextView(context);
		txt.setLayoutParams(lllp2);
		txt.setTextColor(0xFFFFFFFF);
		txt.setText(msg);
		
		layButton.addView(btnCancel, lllp1);
		layButton.addView(btnOk, lllp1);
		
		layBack.addView(txt);
		layBack.addView(layButton);
		
		btnOk.setOnClickListener(onClickOk);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		return layBack;
	}
	
	/**
	 * onClick Rich
	 */
	private OnClickListener onClickOk = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startNotiReceiver();
			finish();
		}
	};
	
	/**
	 * noti receiver
	 */
	private void startNotiReceiver() {
		Prefs prefs = new Prefs(mContext);
		String receiverClass = prefs.getString(PREF_NOTI_RECEIVER);
		receiverClass = receiverClass != null ? receiverClass : "com.pms.sdk.notification";
		
		Intent intent = new Intent(receiverClass);
		intent.putExtras(getIntent().getExtras());
		mContext.sendBroadcast(intent);
		
		// hide notification
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(PushReceiver.NOTIFICATION_ID);
	}
	
	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(0, 0);
	}
}