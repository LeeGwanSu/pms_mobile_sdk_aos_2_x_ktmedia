package com.pms.sdk.push;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.view.BitmapLruCache;

/**
 * push receiver
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {
	
	private static final String USE = "Y";

	private static final String TAG = "[" + PushReceiver.class.getSimpleName() + "]";
	
	// notification id
	public final static int 	NOTIFICATION_ID	= 0x253470;
	private Prefs mPrefs;
	
	private PowerManager pm;
	private PowerManager.WakeLock wl;
	
	private static final int DEFAULT_SHOWING_TIME = 30000;
	
	private Handler mFinishHandler = new Handler();
	
	private Bitmap mPushImage;
	
	/**
	 * gcm key register
	 * @param context
	 * @param senderId
	 */
	public static void gcmRegister(Context context, String senderId) {
		Intent registrationIntent = new Intent(ACTION_REGISTER);
		registrationIntent.setPackage(GSF_PACKAGE);
		registrationIntent.putExtra(KEY_APP, PendingIntent.getBroadcast(context, 0, new Intent(), 0));
		registrationIntent.putExtra(KEY_SENDER, senderId);
		context.startService(registrationIntent);
	}
	
	@Override
	public void onReceive(final Context context, final Intent intent) {
		mPrefs = new Prefs(context);
		
		if (intent.getAction().equals(ACTION_REGISTRATION)) {
			// registration
			CLog.i(TAG + "onReceive:registration");
	    	if (intent.getStringExtra(KEY_GCM_TOKEN) != null) {
	    		// regist gcm key
	    		CLog.d(TAG + "handleRegistration:key=" + intent.getStringExtra(KEY_GCM_TOKEN));
	    		mPrefs.putString(KEY_GCM_TOKEN, intent.getStringExtra(KEY_GCM_TOKEN));
	    	} else {
	    		// error occurred
	    		CLog.i(TAG + "handleRegistration:error=" + intent.getStringExtra("error"));
	    		CLog.i(TAG + "handleRegistration:unregistered=" + intent.getStringExtra("unregistered"));
	    	}
		} else {
			// receive push message
			if (intent.getAction().equals(MQTTService.INTENT_RECEIVED_MSG)) {
				// private server
				String message = intent.getStringExtra(MQTTService.KEY_MSG);
				
				CLog.i(TAG + "onReceive:receive from private server");
				
				// set push info
				try {
					JSONObject msgObj = new JSONObject(message);
					try { intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID)); } catch(Exception e) {e.printStackTrace();}
					try { intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE)); } catch(Exception e) {e.printStackTrace();}
					try { intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE)); } catch(Exception e) {e.printStackTrace();}
					try { intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG)); } catch(Exception e) {e.printStackTrace();}
					try { intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND)); } catch(Exception e) {e.printStackTrace();}
					try { intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG)); } catch(Exception e) {e.printStackTrace();}
					try { intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA)); } catch(Exception e) {e.printStackTrace();}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (intent.getAction().equals(ACTION_RECEIVE)) {
				// gcm
				// key: title, msgType, message, sound, data
				CLog.i(TAG + "onReceive:receive from GCM");
			}
			
			
			if (isImagePush(intent.getExtras())) {
				// image push
				RequestQueue queue = Volley.newRequestQueue(context);
				ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
				
				imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
					@Override
					public void onResponse(ImageContainer response, boolean isImmediate) {
						if (response == null) {
							CLog.e(TAG + "response is null");
							return;
						}
						if (response.getBitmap() == null) {
							CLog.e(TAG + "bitmap is null");
							return;
						}
						mPushImage = response.getBitmap();
						CLog.i(TAG + "imageWidth:" + mPushImage.getWidth());
						onMessage(context, intent);
					}
					@Override
					public void onErrorResponse(VolleyError error) {
						CLog.e(TAG + "onErrorResponse:"+error.getMessage());
						// wrong img url (or exception)
						onMessage(context, intent);
					}
				});
				
			} else {
				// default push
				onMessage(context, intent);
			}
		}
	}
    
	/**
	 * on message (gcm, private msg receiver)
	 * @param context
	 * @param intent
	 */
    private void onMessage(final Context context, Intent intent) {
    	
    	final Bundle extras = intent.getExtras();
        
    	PushMsg pushMsg = new PushMsg(extras);
    	
    	if (pushMsg.msgId == null || pushMsg.notiTitle == null || pushMsg.notiMsg == null || pushMsg.msgType == null) {
    		CLog.i(TAG + "msgId or notiTitle or notiMsg or msgType is null");
    		return;
    	}
    	
    	CLog.i(TAG + pushMsg);
    	
    	// insert (temp) new msg
    	Msg newMsg = new Msg();
    	newMsg.readYn = Msg.READ_N;
    	newMsg.msgGrpCd = "999999";
    	newMsg.expireDate = "0";
    	
    	PMSDB db = PMSDB.getInstance(context);
    	db.insertMsg(newMsg);
		
		// refresh list and badge
    	context.sendBroadcast(new Intent(RECEIVER_PUSH).putExtras(extras));
		
		// push ack
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				HttpResponse response = null;
//				try {
//					HttpClient client = new DefaultHttpClient();
//					HttpGet request = new HttpGet();
//					request.setURI(new URI("http://119.207.76.91/check.html?id=" + msg.msgId + "&recvTime=" + DateUtil.getNowDate() + "&custId=android"));
//					response = client.execute(request);
//					HttpEntity resEntity = response.getEntity();
//					CLog.d(EntityUtils.toString(resEntity));
//				} catch (Exception e) {
//					CLog.e("[MQTT]" + e.getMessage());
//				}
//			}
//		}).run();
		// [temp-end] test private
    	
		// show noti 
    	// ** 수정 **  기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
    	CLog.i(TAG+"NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));
    	
		if (USE.equals(mPrefs.getString(PREF_NOTI_FLAG))
				|| StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			// check push flag
			
			if (USE.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG))
					|| StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
				// screen on
				pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Tag");
				if (!pm.isScreenOn()) {
					wl.acquire();
				}
				
				mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
			} 
			
			CLog.i(TAG+"version code :"+Build.VERSION.SDK_INT);
			
			// execute push noti listener
			PMS pms = PMS.getInstance(context);
	    	if (pms.getOnReceivePushListener() != null) {
	    		if (pms.getOnReceivePushListener().onReceive(context, extras)) {
	    			showNotification(context, extras);
	    		}
	    	} else {
	    		showNotification(context, extras);
	    	}

	    	CLog.i(TAG+"ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));
	    	
	    	if (USE.equals(mPrefs.getString(PREF_ALERT_FLAG))
	    			|| StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
	    		showPopup(context, extras);	
	    	}
		}
    }
    
    /**
     * show notification
     * @param context
     * @param extras
     */
    private void showNotification(Context context, Bundle extras) {
    	
    	// push intent sample
//    	String msgId = extras.getString(KEY_MSG_ID);
//		String title = extras.getString(KEY_TITLE);
//		String msg = extras.getString(KEY_MSG);
//		String msgType = extras.getString(KEY_MSG_TYPE);
//		String sound = extras.getString(KEY_SOUND);
//		String data = extras.getString(KEY_DATA);
    	CLog.i(TAG + "showNotification");
    	if (isImagePush(extras)) {
    		showNotificationImageStyle(context, extras);
    	} else {
    		showNotificationTextStyle(context, extras);
    	}
    }
    
    /**
     * show notification text style
     * @param context
     * @param extras
     */
	private void showNotificationTextStyle(Context context, Bundle extras) {
		CLog.i(TAG + "showNotificationTextStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);
		
		// notification
    	int iconId = mPrefs.getInt(PREF_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_NOTI_ICON) : 0;
    	int largeIconId = mPrefs.getInt(PREF_LARGE_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_LARGE_NOTI_ICON) : 0;
    	
    	NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
    	builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(pushMsg.notiMsg);
		builder.setContentTitle(pushMsg.notiTitle);
		// setting lights color
		builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);
    	
		// set small icon
    	CLog.i(TAG+"small icon :"+iconId);
    	if (iconId > 0) {
    		builder.setSmallIcon(iconId);
    	}
    	
    	// set large icon
    	CLog.i(TAG+"large icon :"+largeIconId);
    	if (largeIconId > 0) {
    		builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
    	}
    	
		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		
		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG))
					|| StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
				try {
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					if (notiSound > 0) {
						Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
			                    + context.getPackageName() + "/" + notiSound);
						builder.setSound(soundUri);
					} else {
						throw new Exception("DEFAULT_SOUND");
					}
				} catch (Exception e) {
					Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

					builder.setSound(uri);
				}
			}
		}
		
		// check vibe mode
		if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) 
				|| StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}
		
		// show notification
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = builder.getNotification();
		notificationManager.notify(NOTIFICATION_ID, notification);
	}
	
	/**
	 * show notification image style
	 * @param context
	 * @param extras
	 */
	@SuppressLint("NewApi")
	private void showNotificationImageStyle(Context context, Bundle extras) {
		CLog.i(TAG + "showNotificationImageStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);
		
		// notification
    	int iconId = mPrefs.getInt(PREF_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_NOTI_ICON) : 0;
    	int largeIconId = mPrefs.getInt(PREF_LARGE_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_LARGE_NOTI_ICON) : 0;
    	
    	Notification.Builder builder = new Notification.Builder(context);
    	builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		builder.setContentIntent(makePendingIntent(context, extras));
		builder.setAutoCancel(true);
		builder.setContentText(pushMsg.notiMsg);
		builder.setContentTitle(pushMsg.notiTitle);
		// setting lights color
		builder.setLights(0xFF00FF00, 1000, 2000);
    	
		// set small icon
    	CLog.i(TAG+"small icon :"+iconId);
    	if (iconId > 0) {
    		builder.setSmallIcon(iconId);
    	}
    	
    	// set large icon
    	CLog.i(TAG+"large icon :"+largeIconId);
    	if (largeIconId > 0) {
    		builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
    	}
    	
		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
		
		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG))
					|| StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
				try {
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					if (notiSound > 0) {
						Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
			                    + context.getPackageName() + "/" + notiSound);
						builder.setSound(soundUri);
					} else {
						throw new Exception("DEFAULT_SOUND");
					}
				} catch (Exception e) {
					Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

					builder.setSound(uri);
				}
			}
		}
		
		// check vibe mode
		if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) 
				|| StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}
		
		if (mPushImage == null) {
			CLog.e(TAG+"mPushImage is null");
		}
		
		// show notification
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification.BigPictureStyle(builder)
				.bigPicture(mPushImage)
				.setBigContentTitle(pushMsg.notiTitle)
				.setSummaryText(pushMsg.notiMsg)
				.build();
		notificationManager.notify(NOTIFICATION_ID, notification);
	}
	
	/**
	 * make pending intent
	 * @param context
	 * @param extras
	 * @return
	 */
	private PendingIntent makePendingIntent(Context context, Bundle extras) {
		// notification
    	String receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER);
    	receiverClass = receiverClass != null ? receiverClass : "com.pms.sdk.notification";
    	
    	// setting push info to intent
		Intent innerIntent = new Intent(receiverClass).putExtras(extras);

		return PendingIntent.getBroadcast(context, 0, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	
	
	/**
	 * show popup (activity)
	 * @param context
	 * @param extras
	 */
	private void showPopup(Context context, Bundle extras) {
		Class<?> pushPopupActivity;
		
		String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);
		
		if (StringUtil.isEmpty(pushPopupActivityName)) {
			pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
		}
		
		try {
			pushPopupActivity = Class.forName(pushPopupActivityName);
		} catch (ClassNotFoundException e) {
			CLog.e(TAG + e.getMessage());
			pushPopupActivity = PushPopupActivity.class;
		}
		CLog.i(TAG+"pushPopupActivity :"+pushPopupActivityName);
		
		Intent pushIntent = new Intent(context, pushPopupActivity);
		pushIntent.setFlags(
				Intent.FLAG_ACTIVITY_MULTIPLE_TASK
				| Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_NO_ANIMATION
//				| Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_NO_HISTORY
				);
		pushIntent.putExtras(extras);
		context.startActivity(pushIntent);
	}
	
	/**
	 * is image push
	 * @param extras
	 * @return
	 */
	private boolean isImagePush(Bundle extras) {
		try {
			if (!PhoneState.isNotificationNewStyle()) {
				throw new Exception("wrong os version");
			}
			String notiImg = extras.getString(KEY_NOTI_IMG);
			CLog.i(TAG + "notiImg:" + notiImg);
			if (notiImg == null || "".equals(notiImg)) {
				throw new Exception("no image type");
			}
			return true;
		} catch (Exception e) {
			CLog.e(TAG + "isImagePush:" + e.getMessage());
			return false;
		}
	}
	
	/**
	 * finish runnable
	 */
	private Runnable finishRunnable = new Runnable() {
		public void run() {
			if (wl != null && wl.isHeld()) {
				wl.release();
			}
		};
	};
	
	
	
	
	/**
	 * show instant view (toast)
	 * @param context
	 * @param msg
	 */
//	private void showInstantView(Context context, String msg) {
//		LayoutInflater mInflater = LayoutInflater.from(context);
//		View v = mInflater.inflate(R.layout.pms_push_instant_view, null);
//		LinearLayout layPushMsg = (LinearLayout) v.findViewById(R.id.pms_lay_push_msg);
//		TextView txtPushTitle = (TextView) v.findViewById(R.id.pms_txt_push_title);
//		TextView txtPushMsg = (TextView) v.findViewById(R.id.pms_txt_push_msg);
//		
//		txtPushMsg.setText(msg);
//		
//		String currentTheme = Prefs.getString(context, Consts.PREF_THEME);
//		if (currentTheme.equals(context.getString(R.string.pms_txt_green))) {
//			layPushMsg.setBackgroundColor(0xCCC1C95E);
//			txtPushTitle.setTextColor(0xFFC1DF7D);
//		} else if (currentTheme.equals(context.getString(R.string.pms_txt_pink))) {
//			layPushMsg.setBackgroundColor(0xCCF695CA);
//			txtPushTitle.setTextColor(0xFFFED6F0);
//		} else if (currentTheme.equals(context.getString(R.string.pms_txt_brown))) {
//			layPushMsg.setBackgroundColor(0xCCD09F76);
//			txtPushTitle.setTextColor(0xFFFDD9B9);
//		} else if (currentTheme.equals(context.getString(R.string.pms_txt_black))) {
//			layPushMsg.setBackgroundColor(0xCC5C5C5C);
//			txtPushTitle.setTextColor(0xFF949494);
//		} else {
//			layPushMsg.setBackgroundColor(0xCCC1C95E);
//			txtPushTitle.setTextColor(0xFFC1DF7D);
//		}
//		
//		txtPushMsg.setTextColor(0xFFFFFFFF);
//		
//		Toast toast = new Toast(context);
//		toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
//		toast.setDuration(Toast.LENGTH_SHORT);
//		toast.setView(v);
//		toast.show();
//	}
}
