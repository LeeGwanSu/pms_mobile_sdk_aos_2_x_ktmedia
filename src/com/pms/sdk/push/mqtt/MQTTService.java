package com.pms.sdk.push.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.FileUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;

/**
 * MQTT Private PUSH Service
 * @author erzisk
 * @since 2013.06.07
 */
public class MQTTService extends Service implements IPMSConsts {

	private static final String TAG = "MQTT";
	
	// intent, key
	public static final String INTENT_RECEIVED_MSG = "org.mosquitto.android.mqtt.MSGRECVD";
	public static final String KEY_MSG	= "org.mosquitto.android.mqtt.MSG";
	
	// preferences info
	private static final String PREF_FILE_NAME = "mqttconnectioninfo.pref";
	private static final String PREF_SERVER_PROTOCOL = "pref_server_protocol";
	private static final String PREF_SERVER_IP = "pref_server_ip";
	private static final String PREF_SERVER_PORT = "pref_server_port";
	private static final String PREF_TOPIC = "pref_topic";
	
	// 
	public static final int MAX_MQTT_CLIENTID_LENGTH = 22;
	
	// mqtt client
	private MqttClient mMqttClient = null;
	// mqtt connection option
	private MqttConnectOptions mMqttConnectOptions;
	
	// mqtt state
	private int status = 0;
	private int STATUS_NOT_CONNECTED = 0;
	private int STATUS_CONNECTING = 1;
	private int STATUS_CONNECTED = 2;
	
	// client id
	private String mClientId;
	private String mServerProtocol;
	private String mServerIp;
	private String mServerPort;
	private String mTopic;
	
	@Override
	public IBinder onBind(Intent intent) {
		Log.d(TAG, "onBind");
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand:" + flags + "," + startId);
		
		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(getApplicationContext()))) {
			SharedPreferences prefs = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);
			mServerProtocol = prefs.getString(PREF_SERVER_PROTOCOL,  "tcp");
			mServerIp = prefs.getString(PREF_SERVER_IP, "119.207.76.91");
			mServerPort = prefs.getString(PREF_SERVER_PORT,  "1883");
//			mTopic = prefs.getString(PREF_TOPIC,  "amail");
			mTopic = PMSUtil.getApplicationKey(getApplicationContext());
			
			if (isOnline() && !isAlreadyConnected() && status == STATUS_NOT_CONNECTED) {
				Log.d(TAG, "onStartCommand:not connected, try (re)connect");
				connectToBroker(mServerProtocol, mServerIp, mServerPort, mTopic);
			} else {
				if (isOnline() && !isAlreadyConnected()) {
					Log.d(TAG, "onStartCommand:already connected");
				} else if (status == STATUS_CONNECTING) {
					Log.d(TAG, "onStartCommand:already request connecting");
				}
			}
		}
		
		return START_STICKY;
	}
	
	@Override
	public void onStart(final Intent intent, int startId)
	{
		Log.d(TAG, "onStart:" + startId);
//		brokerScheme = "ssl";
//		brokerHostName = "bluelemonade.feeltong.me";
//		brokerPort = "10001";
//		topicName = settings.getString("topic",  "amail");
	}
	
	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		try {
			if (mMqttClient != null && mMqttClient.isConnected()) {
				mMqttClient.disconnect();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		super.onDestroy();
	}
	

	/**
	 * connect to broker
	 */
	private synchronized void connectToBroker(final String serverProtocol, final String serverIp, final String serverPort, final String topic) {
		
		final String serverUrl = serverProtocol + "://" + serverIp + ":" + serverPort;
		
		Log.d(TAG, "connectToBroker:serverUrl=" + serverUrl + ",topic=" + topic);
		
		new Thread(new Runnable() {
			public void run() {
				try {
					status = STATUS_CONNECTING;
					
					while (StringUtil.isEmpty(PMSUtil.getAppUserId(getApplicationContext()))) {
						Log.i(TAG, "connectToBroker:appUserId is null, sleep(2000)");
						try {
							Thread.sleep(5000);
						} catch (Exception e) {
						}
					}
					mClientId = PMSUtil.getAppUserId(getApplicationContext());
					Log.d(TAG, "connectToBroker:clientId=" + mClientId);
					
					PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
					WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Tag");
					if (!pm.isScreenOn()) {
						wl.acquire();
					}
					
					if (mMqttClient == null) {
						mMqttClient = new MqttClient(serverUrl, mClientId, new MemoryPersistence());
//						mMqttClient = new MqttClient(serverUrl, mClientId, mqttClientPersistence);
//						mMqttClient = new MqttClient(serverUrl, mClientId);
					}

					// default keepalive : 60, timeout : 30
					mMqttConnectOptions = new MqttConnectOptions();
					mMqttConnectOptions.setKeepAliveInterval(400);
					
					mMqttConnectOptions.setUserName(mClientId);
					mMqttConnectOptions.setPassword(topic.toCharArray());
//					mMqttConnectOptions.setConnectionTimeout(30);
					if ("ssl".equals(serverProtocol)) {
						mMqttConnectOptions.setSocketFactory(SelfSignedSocketFactory.selfSignedSocketFactory("TLS"));
					}
					mMqttClient.setCallback(mqttCallback);
					
					Log.d(TAG, "connectToBroker:try connect to broker");
					mMqttClient.connect(mMqttConnectOptions);
					if (mMqttClient.isConnected()) {
						if (!isAlreadySubscribe()) {
							Log.d(TAG, "[connectToBroker] subscribe");
							mMqttClient.subscribe(topic);
						}
					}
					Log.d(TAG, "connectToBroker:succsess connect, topic=" + topic);
					
					if (wl.isHeld()) {
						wl.release();
					}
					
					status = STATUS_CONNECTED;
				} catch (Exception e) {
					e.printStackTrace();
					Log.d(TAG, "connectToBroker:failed to connect for some reason, " + e.getMessage());
					mMqttClient = null;
					status = STATUS_NOT_CONNECTED;
				}
			}
		}).start();
	}
	
	
	/**
	 * broadcast received message
	 * @param message
	 */
	private void broadcastReceivedMessage(String message){
		Log.d(TAG, "broadcastReceivedMessage:message=" + message);
		
		Intent broadcastIntent = new Intent(INTENT_RECEIVED_MSG);
		broadcastIntent.addCategory(getApplication().getPackageName());
		
		broadcastIntent.putExtra(KEY_MSG, message);
		sendBroadcast(broadcastIntent);
	}
	

	/************************************************************************/
	/*	METHODS - internal utility methods								*/
	/************************************************************************/

	/**
	 * is online
	 * @return
	 */
	private boolean isOnline() {
		
		ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
		if(cm.getActiveNetworkInfo() != null &&
		   cm.getActiveNetworkInfo().isAvailable() &&
		   cm.getActiveNetworkInfo().isConnected())
		{
			return true;
		}

		return false;
	}

	/*
	 * Checks if the MQTT client thinks it has an active connection
	 */
	private boolean isAlreadyConnected() {
		return ((mMqttClient != null) && mMqttClient.isConnected());
	}
	
	private boolean isAlreadySubscribe() {
		return false;
	}
	
	/**
	 * mqtt call back
	 */
	private MqttCallback mqttCallback = new MqttCallback() {
		@Override
		public void messageArrived(String mqttTopic, MqttMessage mqttMessage) throws Exception {
			String message = new String(mqttMessage.getPayload());
			Log.d(TAG,  "messageArrived:message=" + message);
			broadcastReceivedMessage(message);
		}
		
		@Override
		public void deliveryComplete(IMqttDeliveryToken arg0) {
			Log.d(TAG, "deliveryComplete");
		}
		
		@Override
		public void connectionLost(Throwable arg0) {
			Log.d(TAG, "connectionLost");
			status = STATUS_NOT_CONNECTED;
			
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Tag");
			
			FileUtil fu = new FileUtil();
			try {
				ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
				
				String data= String.format("[" + DateUtil.getNowDate() + "][mqttCallback:connectionLost] " +
						"pm.isScreenOn():%s, wl.isHeld():%s, cm.getActiveNetworkInfo().isAvailable():%s, cm.getActiveNetworkInfo().isConnected():%s" +
						"3g:%s, wifi:%s\n",
						pm.isScreenOn() + "",
						wl.isHeld() + "",
						cm.getActiveNetworkInfo().isAvailable() + "",
						cm.getActiveNetworkInfo().isConnected() + "",
						cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting() + "",
						cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting() + "");
				
				fu.writeToSdCard("pms_file", "log.txt", data);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if (isOnline()) {
				if (!isAlreadyConnected()) {
					connectToBroker(mServerProtocol, mServerIp, mServerPort, mTopic);
				} else {
					Log.d(TAG, "connectionLost:isAlreadyConnected()=true");
				}
				
			} else {
				Log.d(TAG, "connectionLost:isOnline()=false");
			}
		}
	};
}
