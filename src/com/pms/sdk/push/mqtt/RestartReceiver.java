package com.pms.sdk.push.mqtt;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.PMSUtil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Private PUSH Restart Receiver
 * @author erzisk
 *
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final String TAG = "MQTT";
	private static final int DELAY_SECOND = 400;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "RestartReceiver:onReceive()");
		
		if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
			context.startService(new Intent(context, MQTTService.class));
			setAlarmRestart(context);
		}
	}
	
	/**
	 * set alarm restart
	 * @param context
	 */
	private void setAlarmRestart(Context context) {
		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		Intent Intent = new Intent("ACTION_MQTT_PING");
        PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, Intent, 0);
        am.set(AlarmManager.RTC, System.currentTimeMillis() + (DELAY_SECOND * 1000), pIntent);
	}
}
