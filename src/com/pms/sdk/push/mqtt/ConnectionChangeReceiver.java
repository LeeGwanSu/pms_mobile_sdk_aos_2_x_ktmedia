package com.pms.sdk.push.mqtt;

import com.pms.sdk.common.util.CLog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

/**
 * Connection Change Receiver
 * @author erzisk
 * @since 2013.07.23
 */
public class ConnectionChangeReceiver extends BroadcastReceiver {
	
	private static final String TAG = "[" + ConnectionChangeReceiver.class.getSimpleName() + "]";

	@Override
	public void onReceive(Context context, Intent intent) {
		CLog.i(TAG + "onReceive");
		
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			context.sendBroadcast(new Intent(context, RestartReceiver.class));
		}
	}
}
