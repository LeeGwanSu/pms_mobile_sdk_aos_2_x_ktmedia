package com.pms.sdk.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.SystemClock;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.common.security.SA2Dec;
import com.pms.sdk.common.security.SA2Enc;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;

/**
 * APIManager
 * @author erzisk
 * @since 2013.05.09
 */
public class APIManager implements IPMSConsts {
	private final String TAG = "[" + APIManager.class.getName() + "]";
	
	private Context mContext;
	private Prefs mPrefs;
	private RequestQueue mRequestQueue;
	private JSONObject tempParams;
	
	public APIManager(Context context) {
		this.mContext = context;
		this.mPrefs = new Prefs(mContext);
		this.mRequestQueue = Volley.newRequestQueue(mContext);
	}
	
	@SuppressWarnings("deprecation")
	public synchronized void call(final String url, final JSONObject params, final APICallback apiCallback) throws Exception {
		CLog.i(TAG + "API:request=" + url);
		try {
			// check network available
			if (!PhoneState.getWifiState(mContext) && !PhoneState.get3GState(mContext)) {
				throw new APIException(CODE_CONNECTION_ERROR, "network not available");
			}
			
			// check url is null
			if (url == null || "".equals(url)) {
				throw new APIException(CODE_URL_IS_NULL, "url is null");
			}
			String apiServerUrl = PMSUtil.getServerUrl(mContext);
			CLog.d(TAG + " SERVER_URL + url = " + apiServerUrl + url);
			
			// check param is null
			if (params == null || "".equals(params.toString())) {
				throw new APIException(CODE_PARAMS_IS_NULL, "params are null");
			}
			CLog.d(TAG + " params(json) = " + params.toString());
			
			// encrypt
			final String encryptedParam = encrypt(url, params.toString());
			CLog.d(TAG + " encryptedParam:" + encryptedParam);
			
			
			// call http
			tempParams = params;
			StringRequest stringRequest = new StringRequest(
					Method.POST,
					apiServerUrl + url,
					new Response.Listener<String>() {
						@Override
						public void onResponse(String object) {
							proccessResult(url, object, apiCallback);
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError arg0) {
							arg0.printStackTrace();
						}
					}) {
				@Override
				protected Map<String, String> getParams() throws AuthFailureError {
					// set params
					Map<String, String> map = new HashMap<String, String>();
			        map.put(KEY_API_DEFAULT, encryptedParam);
					CLog.d(TAG + " params(map):" + map.toString());
					return map;
				}
			};
			mRequestQueue.add(stringRequest);
		} catch (Exception e) {
			CLog.e(TAG + e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	/**
	 * processResult
	 * @param context
	 * @param url
	 * @param object
	 * @param apiCallback
	 */
	private boolean proccessResult(final String url, final String object, final APICallback apiCallback) {
		try {
			// check result string
			if (object == null || "".equals(object)) {
				throw new APIException(CODE_NOT_RESPONSE, "not response");
			}
			CLog.d(TAG + " result:" + object);
			
			// decrypt
			String decryptedString = decrypt(url, object.toString());
			CLog.d(TAG + " result decrypted String:" + decryptedString);
			
			// check live session
			long nowTime = SystemClock.currentThreadTimeMillis();
			
			if (url.indexOf(API_DEVICE_CERT) < 0) {
				// session time out
				long lastApiTime = Long.parseLong(mPrefs.getString(PREF_LAST_API_TIME));
				CLog.i(TAG + " diffTime:" + (nowTime - lastApiTime));
				if (lastApiTime + EXPIRE_RETAINED_TIME < nowTime) {
					throw new APIException(CODE_WRONG_SESSION, "expired session");
				}
			}
			mPrefs.putString(PREF_LAST_API_TIME, nowTime + "");
			
			// set result
			JSONObject result = new JSONObject(decryptedString);
			String code = result.getString(KEY_API_CODE);
			String msg = result.getString(KEY_API_MSG);
			
			// check result code
			if (CODE_NULL_PARAM.equals(code) ||
					CODE_WRONG_PARAM.equals(code) ||
					CODE_DECRPYT_FAIL.equals(code) ||
					CODE_INNER_ERROR.equals(code) ||
					CODE_EXTRA_ERROR.equals(code) ||
					CODE_WRONG_SESSION.equals(code)) {
				// server error
				throw new APIException(code, msg);
			} else {
				// success or etc error
				if (apiCallback != null) {
					apiCallback.response(code, result);
				}
				CLog.i(TAG + "API:success");
				return true;
			}
		} catch (Exception e) {
			CLog.e(TAG + e.getMessage());
			e.printStackTrace();
			if (e instanceof APIException && CODE_WRONG_SESSION.equals(((APIException) e).getCode())) {
				// wrong session
				
				new DeviceCert(mContext).request(null, new APICallback() {
					@Override
					public void response(String code, JSONObject json) {
						try {
							if (CODE_SUCCESS.equals(code)) {
								call(url, tempParams, apiCallback);
							}
						} catch(Exception e) {
							CLog.e(TAG + e.getMessage());
							e.printStackTrace();
						}
					}
				});
			}
			return false;
		}
	}
	
	/**
	 * encrypt
	 * @param url
	 * @param params
	 * @return
	 * @throws JSONException
	 */
	private String encrypt(String url, String params) throws Exception {
		if (isDefaultEncKey(url)) {
			return SA2Enc.encode(params, DEFAULT_ENC_KEY);
		} else {
			JSONObject encryptedJobj = new JSONObject();
			String appUserId = PMSUtil.getAppUserId(mContext);
			String encKey = PMSUtil.getEncKey(mContext);
			if (StringUtil.isEmptyArr(new String[]{appUserId, encKey})) {
				throw new Exception("appUserId or encKey is null, have to call deviceCert before");
			} else {
				encryptedJobj.put(KEY_APP_USER_ID, appUserId);
				encryptedJobj.put(KEY_ENC_PARAM, SA2Enc.encode(params, encKey));
				return SA2Enc.encode(encryptedJobj.toString(), DEFAULT_ENC_KEY);
			}
		}
	}
	
	/**
	 * decrypt
	 * @param url
	 * @param object
	 * @return
	 * @throws Exception
	 */
	private String decrypt(String url, String object) throws Exception {
		String result;
		try {
			result = SA2Dec.decrypt(object, getEncKey(url));
			if (result == null) {
				result = object;
			}
		} catch (Exception e) {
			// wrong session인 경우 암호화지 않은 상태에서 데이터가 넘어온다.
			result = object;
		}
		return result;
	}
	
	/**
	 * get enc key
	 * @param url
	 * @return
	 */
	private String getEncKey(String url) {
		if (isDefaultEncKey(url)) {
			return DEFAULT_ENC_KEY;
		} else {
			return PMSUtil.getEncKey(mContext);
		}
	}
	
	/*
	 * is default encrypt key
	 */
	private boolean isDefaultEncKey(String url) {
		if (url.indexOf(API_DEVICE_CERT) > -1) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * https
	 * @param url
	 * @return
	 */
	private boolean isHttps(String url) {
		return false;
	}
	
	/**
	 * is multipart
	 * @param url
	 * @return
	 */
	private boolean isMultipart(String url) {
		return false;
	}
	
	/**
	 * api result callback
	 * @author erzisk
	 */
	public interface APICallback {
		void response(String code, JSONObject json);
	}
}
