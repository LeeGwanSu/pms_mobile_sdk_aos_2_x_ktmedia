package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.view.Badge;

public class DeviceCert extends BaseRequest {
	
	private static final String TAG = "[" + DeviceCert.class.getName() + "]";
	
	public DeviceCert(Context context) {
		super(context);
	}
	
	/**
	 * get param
	 * @return
	 */
	public JSONObject getParam(JSONObject userData) {
		JSONObject jobj;
		
		try {
			jobj = new JSONObject();
			
			// new version
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
//			jobj.put("uuid", PhoneState.getGlobalDeviceToken(mContext));
			jobj.put("uuid", PMSUtil.getUUID(mContext));
			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
			jobj.put("custId", PMSUtil.getCustId(mContext));
			jobj.put("appVer", PhoneState.getAppVersion(mContext));
			jobj.put("os", "A");
			jobj.put("osVer", PhoneState.getOsVersion());
			jobj.put("device", PhoneState.getDeviceName());
			jobj.put("otnId", PMSUtil.getOtnId(mContext));
			
			if (userData != null) {
				jobj.put("userData", userData);
			}
			
			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * request
	 * @param apiCallback
	 */
	public void request(final JSONObject userData, final APICallback apiCallback) {
		new AsyncTask<String, String, Boolean>() {
			@Override
			protected Boolean doInBackground(String... params) {
				try {
					String custId = PMSUtil.getCustId(mContext);
					// 앱이 처음 수행 됐을 때, push token을 받아오기 전 까지 device cert를 수행하지 않게함
					PhoneState.createDeviceToken(mContext); //2017.09.14 UUID 생성 로직 수정 됨

					int checkCnt = 0;
					while (checkCnt < 15 &&
							(StringUtil.isEmpty(PMSUtil.getGCMToken(mContext)) || NO_TOKEN.equals(PMSUtil.getGCMToken(mContext)))) {
						try {
							CLog.i(TAG + "deviceCert:pushToken is null, sleep(2000)");
							checkCnt++;
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					if (StringUtil.isEmpty(PMSUtil.getGCMToken(mContext))) {
						// check push token
						CLog.i(TAG + "DeviceCert:no push token");
						mPrefs.putString(KEY_GCM_TOKEN, NO_TOKEN);
					}
					
					if (!StringUtil.isEmpty(PMSUtil.getCustId(mContext))
							&& !custId.equals(PMSUtil.getCustId(mContext))) {
						// 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
						CLog.i(TAG + "DeviceCert:new user");
						mDB.deleteAll();
					}
					mPrefs.putString(PREF_CUST_ID, custId);
					
					CLog.i(TAG + "DeviceCert:validate ok");
					
					apiManager.call(API_DEVICE_CERT, getParam(userData), new APICallback() {
						@Override
						public void response(String code, JSONObject json) {
							requiredResultProc(json);
							if (apiCallback != null) {
								apiCallback.response(code, json);
							}
						}
					});
					
					return true;
				} catch(Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		}.execute();
	}
	
	/**
	 * required result proccess
	 * @param json
	 */
	private boolean requiredResultProc(JSONObject json) {
		try {
			PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
			PMSUtil.setEncKey(mContext, json.getString("encKey"));
			
			// set msg flag
			mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			mPrefs.putString(PREF_CONTENT_NOTI_FLAG, json.getString("contentsNotiFlag"));
			mPrefs.putString(PREF_EVENT_NOTI_FLAG, json.getString("eventNotiFlag"));
			
			// set badge
			Badge.getInstance(mContext).updateBadge(json.getString("newMsgCnt"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
