package com.pms.sdk.api;

/**
 * 
 * @author erzisk
 * @since 2013.05.09
 */
public class APIException extends Exception{

	private static final long serialVersionUID = -5638417999120184800L;
	
	private String mCode;
	private String mMsg;
	
	public APIException(String code, String msg) {
		this.mCode = code;
		this.mMsg = msg;
	}
	
	@Override
	public String getMessage() {
		return "[error] code : " + mCode + ", msg : " + mMsg;
	}
	
	public String getCode() {
		return mCode;
	}

}
